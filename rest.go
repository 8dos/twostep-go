package twostep

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"syscall"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// Response model
type Response struct {
	Status  int                    `json:"status"`
	Success bool                   `json:"success"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
	//ErrorCode string                 `json:"error_code"`
}

func (c *Client) get(path string, result interface{}) error {
	req, err := c.newRequest("GET", path, nil)
	if err != nil {
		return err
	}
	return c.execute(req, result)
}

func (c *Client) post(path string, params *Parameters, result interface{}) error {
	req, err := c.newRequest("POST", path, params)
	if err != nil {
		return err
	}
	return c.execute(req, result)
}

func (c *Client) put(path string, params *Parameters, result interface{}) error {
	req, err := c.newRequest("PUT", path, params)
	if err != nil {
		return err
	}
	return c.execute(req, result)
}

func (c *Client) delete(path string, result interface{}) error {
	req, err := c.newRequest("DELETE", path, nil)
	if err != nil {
		return err
	}
	return c.execute(req, result)
}

func (c *Client) newRequest(method, path string, params *Parameters) (*http.Request, error) {
	token, err := c.getToken()
	if err != nil {
		return nil, err
	}

	var body io.ReadWriter
	if params != nil {
		body, err = params.Encode()
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, c.apiURL+path, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+token.AccessToken)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", userAgent)
	if params != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	return req, nil
}

func (c *Client) execute(req *http.Request, result interface{}) error {
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return checkConnectError(err)
	}
	defer closeResponse(resp)

	if resp.StatusCode == 200 || resp.StatusCode == 201 {
		err = parseResponse(resp, result)
	} else {
		err = parseErrorResponse(resp)
	}
	return err
}

func checkConnectError(err error) error {
	switch t := err.(type) {
	case *url.Error:
		if err, ok := t.Err.(net.Error); ok && err.Timeout() {
			return ErrConnectionTimedout
		}
		if opErr, ok := t.Err.(*net.OpError); ok {
			if sysErr, ok := opErr.Err.(*os.SyscallError); ok {
				if sysErr.Err == syscall.ECONNREFUSED {
					return ErrConnectionRefused
				}
			} else {
				//NOTE: Not sure if this is correct!
				return ErrUknownHost
			}
		}
		/* Pretty sure this only occur during read/write and not connect.....
		case net.Error:
		if t.Timeout() {
			return errors.New("Connection timed out")
		}*/
	}
	return err
}

func closeResponse(resp *http.Response) {
	err := resp.Body.Close()
	if err != nil {
	}
}

func parseResponse(resp *http.Response, result interface{}) error {
	if resp.Body == nil {
		return newError(resp.StatusCode, "Server returned no data")
	}
	body, _ := ioutil.ReadAll(resp.Body)
	err := json.Unmarshal(body, result)
	if err != nil {
		err = newError(resp.StatusCode, "Unexpected response from server")
	}
	return err
}

func parseErrorResponse(resp *http.Response) error {
	if resp.Body == nil {
		return newError(resp.StatusCode, "Server returned no data")
	}

	body, _ := ioutil.ReadAll(resp.Body)
	var err error
	switch resp.StatusCode {
	case 500:
		err = newError(resp.StatusCode, "Internal server error")
	case 502:
		err = newError(resp.StatusCode, "Bad gateway")
	case 503:
		err = newError(resp.StatusCode, "Service unavailable")
	}
	if err != nil {
		return err
	}

	var result Response
	err = json.Unmarshal(body, &result)
	if err != nil {
		err = newError(resp.StatusCode, "Unexpected response from server")
	} else if len(result.Message) == 0 {
		err = newError(resp.StatusCode, "Unexpected response from server")
	} else {
		err = newError(resp.StatusCode, result.Message)
	}
	return err
}

// getToken gets a token from TokenStore, new token from server, or refreshed token from server.
func (c *Client) getToken() (*Token, error) {
	var token *Token

	if c.tokenStore != nil {
		// Try to load token from store
		token = c.tokenStore.Load()
	}
	if token != nil && token.isValid() {
		if token.isExpired() {
			// Use existing refresh_token to get a new token
			return c.refreshToken(token)
		}
		return token, nil // got a token
	}

	// Get a new token from server
	return c.newToken()
}

// newToken retrieves an access_token if needed
func (c *Client) newToken() (*Token, error) {
	payload := NewParameters()
	payload.Add("client_id", c.clientID)
	payload.Add("client_secret", c.clientSecret)
	payload.Add("grant_type", "client_credentials")
	body, err := payload.Encode()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", c.apiURL+"/auth/token", body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", userAgent)
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, checkConnectError(err)
	}

	if resp.StatusCode != 200 {
		return nil, parseErrorResponse(resp)
	}

	token := new(Token)
	err = parseResponse(resp, token)
	if err != nil {
		return nil, err
	}

	if c.tokenStore != nil {
		c.tokenStore.Save(token)
	}

	return token, nil
}

// refreshToken obtains a new access_token using the refresh_token if the current
// access_token has expired.
func (c *Client) refreshToken(oldToken *Token) (*Token, error) {
	payload := NewParameters()
	payload.Add("client_id", c.clientID)
	payload.Add("client_secret", c.clientSecret)
	payload.Add("grant_type", "refresh_token")
	payload.Add("refresh_token", oldToken.RefreshToken)

	body, err := payload.Encode()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", c.apiURL+"/auth/token", body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", userAgent)
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, checkConnectError(err)
	}

	if resp.StatusCode != 200 {
		return nil, parseErrorResponse(resp)
	}

	token := new(Token)
	err = parseResponse(resp, token)
	if err != nil {
		return nil, err
	}

	if c.tokenStore != nil {
		c.tokenStore.Save(token)
	}

	return token, nil
}

// isValid checks if a token is valid.
func (t *Token) isValid() bool {
	if len(t.AccessToken) == 0 || len(t.RefreshToken) == 0 {
		return false
	}
	return true
}

// isExpired checks if a token is expired.
func (t *Token) isExpired() bool {
	token, _ := jwt.Parse(t.AccessToken, nil) // no signature verification
	if token != nil {
		claims, ok := token.Claims.(jwt.MapClaims)
		if ok {
			now := time.Now().Unix()
			var exp int64
			if v, ok := claims["exp"]; ok {
				exp = int64(v.(float64))
			}
			if exp > now {
				return false // not expired
			}
		}
	}
	return true // invalid or expired
}
