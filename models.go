package twostep

// User model
type User struct {
	ID          int      `json:"id"`
	Email       []string `json:"email"`
	Phone       string   `json:"phone"`
	CountryCode int      `json:"country_code"`
	Registered  bool     `json:"registered"`
	Confirmed   bool     `json:"confirmed"`
	Devices     []string `json:"devices"`
}

// Receipt model
type Receipt struct {
	Phone   string `json:"phone"`
	Ignored bool   `json:"ignored"`
	Device  string `json:"device"`
}

// Secret model
type Secret struct {
	Secret string `json:"secret"`
	Issuer string `json:"issuer"`
	Label  string `json:"label"`
	QRcode string `json:"qr_code"`
}

// Notification model
type Notification struct {
	UUID      string `json:"uuid"`
	CreatedAt string `json:"created_at"`
	Status    string `json:"status"`
}

// NotificationStatus model
type NotificationStatus struct {
	UserID    int    `json:"user_id"`
	UUID      string `json:"uuid"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	ExpiresIn int    `json:"expires_in"`
	Status    string `json:"status"`
}
