package twostep

import (
	"errors"
	"net/http"
	"time"
)

const (
	version        = "0.6.1"
	liveURL        = "https://api.twostep.io"
	sandboxURL     = "http://sandbox-api.twostep.io" //"https://sandbox-api.twostep.io"
	defaultTimeout = time.Second * 30
)

var (
	userAgent = "twostep-go/" + version
)

// Connection errors
var (
	ErrConnectionTimedout = errors.New("Connection timed out")
	ErrConnectionRefused  = errors.New("Connection refused")
	ErrUknownHost         = errors.New("Unknown host")
)

// Token : tokens for using API
type Token struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	Scope        string `json:"scope"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
}

// TokenStore model
type TokenStore interface {
	Load() *Token
	Save(*Token)
}

// Config model
type Config struct {
	ClientID     string
	ClientSecret string
	APIUrl       string
	Sandbox      bool
	Timeout      time.Duration // must be 1 - 180 seconds
	TokenStore   TokenStore
}

// Client encapsulates the twostep functions - must be created with New()
type Client struct {
	httpClient   *http.Client
	apiURL       string
	clientID     string
	clientSecret string
	tokenStore   TokenStore
}

// NewClient creates a new Client with the provided config.
func NewClient(config Config, httpClient *http.Client) (*Client, error) {
	if len(config.ClientID) == 0 {
		return nil, errors.New("No Client ID provided")
	}

	if len(config.ClientSecret) == 0 {
		return nil, errors.New("No Client secret provided")
	}

	timeout := defaultTimeout
	if config.Timeout >= time.Second*1 && config.Timeout <= time.Second*180 {
		timeout = config.Timeout
	}

	c := &Client{
		clientID:     config.ClientID,
		clientSecret: config.ClientSecret,
	}

	if len(config.APIUrl) > 0 {
		c.apiURL = config.APIUrl
	} else if config.Sandbox {
		c.apiURL = sandboxURL
	} else {
		c.apiURL = liveURL
	}

	if config.TokenStore != nil {
		c.tokenStore = config.TokenStore
	}

	if httpClient == nil {
		c.httpClient = &http.Client{
			Timeout: timeout,
		}
	} else {
		c.httpClient = httpClient
	}

	return c, nil
}
