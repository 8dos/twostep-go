package twostep

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	clientID     = "12345"
	clientSecret = "67890"
)

var (
	testPhoneNumber string
	testFromName    string
	conf            Config
)

type tokenStore struct {
	token *Token // this is our "secret" token storage
}

//TODO: load token from secure store here
//t := new(Token)
//t.AccessToken = "1234"
//t.RefreshToken = "9999"
//return t
func (ts tokenStore) Load() *Token {
	return ts.token
}

//TODO: save tokens to secure store here
func (ts tokenStore) Save(token *Token) {
	ts.token = token
}

func init() {
	testPhoneNumber = "+12125551234"
	testFromName = "Objectia"

	conf = Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Sandbox:      true,
		TokenStore:   tokenStore{},
	}
}

func Test_Client(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_EmptyConfig(t *testing.T) {
	cli, err := NewClient(Config{}, nil)
	assert.Error(t, err)
	assert.Nil(t, cli)
}

func Test_Client_No_ClientID(t *testing.T) {
	cli, err := NewClient(Config{ClientID: "", ClientSecret: clientSecret}, nil)
	assert.Error(t, err)
	assert.Nil(t, cli)
}
func Test_Client_No_ClientSecret(t *testing.T) {
	cli, err := NewClient(Config{ClientID: clientID, ClientSecret: ""}, nil)
	assert.Error(t, err)
	assert.Nil(t, cli)
}

func Test_Client_API_URL(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		APIUrl:       "http://localhost:4000",
	}
	cli, err := NewClient(cnf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_Sandbox_URL(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Sandbox:      true,
	}
	cli, err := NewClient(cnf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_Live_URL(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Sandbox:      false,
	}
	cli, err := NewClient(cnf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_Timeout(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Timeout:      time.Second * 10,
	}
	cli, err := NewClient(cnf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_Large_Timeout(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Timeout:      time.Second * 1000,
	}
	cli, err := NewClient(cnf, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}

func Test_Client_Http_Client(t *testing.T) {
	cnf := Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
	}

	httpCli := &http.Client{
		Timeout: 10,
	}

	cli, err := NewClient(cnf, httpCli)
	assert.NoError(t, err)
	assert.NotNil(t, cli)
}
