package main

import (
	"fmt"
	"os"

	twostep "github.com/objectia/twostep-go"
)

func main() {
	apiKey := os.Getenv("TWOSTEP_API_KEY")
	client, err := twostep.NewClient(apiKey, nil)

	secret, err = client.Init("SHA1")
	if err != nil {
		//logError("Failed to initialize twostep.io client")
	}

	issuer := "mycompany"
	user := "joey@user.com"

	// Generate QR code url ...
	url, err := client.QRUrl(secret, issuer, user)
	if err != nil {
		//logError("Failed to get QR url")
	}
	fmt.Println("URL:", url)

	// ... or get QR image bytes (PNG)
	image, err := client.QRImage(secret, issuer, user)
	if err != nil {
		//logError("Failed to get QR image")
	}
	fmt.Println("Image:", image)

	// Validate login
	code := "<from user input>"
	err = client.Validate(secret, code)
	if err != nil {
		//logError("Failed to validate code")
	}
}
