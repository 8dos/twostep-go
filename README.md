# twostep-go 
[![Build Status](https://travis-ci.org/objectia/twostep-go.svg?branch=master)](https://travis-ci.org/objectia/twostep-go) 
[![codecov](https://codecov.io/gh/objectia/twostep-go/branch/master/graph/badge.svg)](https://codecov.io/gh/objectia/twostep-go)

Go API client for twostep.io

--- UNDER DEVELOPMENT ---


## Requirements

go get github.com/dgrijalva/jwt-go