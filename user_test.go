package twostep

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CreateUser(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	params := NewParameters()
	params.Add("email", "joe@example.com")
	params.Add("phone", "+12125551234")
	params.Add("country_code", 1)
	params.Add("send_install_link", false)

	user, err := cli.CreateUser(params)
	assert.NoError(t, err)
	if user != nil {
		assert.NotEmpty(t, user.ID)
	}
}

func Test_GetUser(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	user, err := cli.GetUser(123456)
	assert.NoError(t, err)
	if user != nil {
		assert.Equal(t, 370, user.CountryCode)
	}

	_, err = cli.GetUser(0)
	assert.Error(t, err)
	assert.NotEmpty(t, err.Error())
}

func Test_RequestSMS(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	/*params := NewParameters()
	params.Add("force", false)
	params.Add("locale", "en")

	resp, err := cli.RequestSMS(123456, params)*/

	resp, err := cli.RequestSMS(123456, nil)
	assert.NoError(t, err)
	if resp != nil {
		assert.Equal(t, "+370-XXX-XXX-XX15", resp.Phone)
	}
}

func Test_RequestCall(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	resp, err := cli.RequestCall(123456, nil)
	assert.NoError(t, err)
	if resp != nil {
		assert.Equal(t, "+370-XXX-XXX-XX15", resp.Phone)
	}
}

func Test_RemoveUser(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	err = cli.RemoveUser(0)
	assert.Error(t, err)

	err = cli.RemoveUser(123456)
	assert.NoError(t, err)
}

func Test_CreateSecret(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	params := NewParameters()
	params.Add("label", "Me")

	secret, err := cli.CreateSecret(123456, params)
	assert.NoError(t, err)
	assert.NotNil(t, secret)
}

func Test_Verify(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	code := "475636"

	err = cli.VerifyCode(123456, code)
	assert.NoError(t, err)
}

/*
func Test_GetQRUrl(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	secret := "YRHXBZ4BUN5HY5JB2NYCBESOYUQMZV3E"
	issuer := "You"
	user := "Me"

	url, err := cli.GetQRUrl(secret, issuer, user)
	assert.NoError(t, err)
	assert.NotEmpty(t, url)
	fmt.Println("URL:", url)
}

func Test_GetQRImage(t *testing.T) {
	cli, err := NewClient(conf, nil)
	assert.NoError(t, err)

	secret := "YRHXBZ4BUN5HY5JB2NYCBESOYUQMZV3E"
	issuer := "You"
	user := "Me"

	img, err := cli.GetQRImage(secret, issuer, user)
	assert.NoError(t, err)
	assert.NotEmpty(t, img)
}
*/
