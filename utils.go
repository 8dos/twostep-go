package twostep

import (
	"encoding/json"
)

// Convert from a map to a struct
func fromMap(data map[string]interface{}, result interface{}) error {
	m, _ := json.Marshal(data)
	return json.Unmarshal(m, &result)
}
