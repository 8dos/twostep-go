package twostep

import (
	"fmt"
	"net/url"
)

// CreateUser creates a new user.
// Optional patameters:
// - send_install_link bool
func (c *Client) CreateUser(params *Parameters) (*User, error) {
	var resp Response
	err := c.post("/v1/users", params, &resp)
	if err != nil {
		return nil, err
	}

	user := &User{}
	err = fromMap(resp.Data, user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetUser returns the user for the given user id.
func (c *Client) GetUser(userID int) (*User, error) {
	var resp Response
	err := c.get(fmt.Sprintf("/v1/users/%d", userID), &resp)
	if err != nil {
		return nil, err
	}

	user := &User{}
	err = fromMap(resp.Data, user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// RequestSMS sends a one-time password via SMS message.
// Optional parameters:
// - force bool
// - locale string
func (c *Client) RequestSMS(userID int, params *Parameters) (*Receipt, error) {
	var resp Response
	err := c.post(fmt.Sprintf("/v1/users/%d/sms", userID), params, &resp)
	if err != nil {
		return nil, err
	}

	sms := &Receipt{}
	err = fromMap(resp.Data, sms)
	if err != nil {
		return nil, err
	}

	return sms, nil
}

// RequestCall sends a one-time password via voice call.
// Optional parameters:
// - force bool
// - locale string
func (c *Client) RequestCall(userID int, params *Parameters) (*Receipt, error) {
	var resp Response
	err := c.post(fmt.Sprintf("/v1/users/%d/call", userID), params, &resp)
	if err != nil {
		return nil, err
	}

	sms := &Receipt{}
	err = fromMap(resp.Data, sms)
	if err != nil {
		return nil, err
	}

	return sms, nil
}

// RemoveUser deletes the user with the specified user id.
func (c *Client) RemoveUser(userID int) error {
	var resp Response
	err := c.delete(fmt.Sprintf("/v1/users/%d", userID), &resp)
	if err != nil {
		return err
	}
	return nil
}

// CreateSecret creates a random secret using the SHA1 algorithm.
// Optional parameters:
// - label string
func (c *Client) CreateSecret(userID int, params *Parameters) (*Secret, error) {
	var resp Response
	err := c.post(fmt.Sprintf("/v1/users/%d/secret", userID), params, &resp)
	if err != nil {
		return nil, err
	}

	secret := &Secret{}
	err = fromMap(resp.Data, secret)
	if err != nil {
		return nil, err
	}

	return secret, nil
}

// VerifyCode checks the validity of a verification code.
func (c *Client) VerifyCode(userID int, code string) error {
	var result Response
	return c.get(fmt.Sprintf("/v1/users/%d/verify/%s", userID, url.QueryEscape(code)), &result)
}

/*

// GetQRUrl -
func (c *Client) GetQRUrl(secret, issuer, user string) (string, error) {
	baseURL, err := url.Parse(c.apiURL + "/v1/totp/qr")
	if err != nil {
		return "", err
	}
	query := url.Values{}
	query.Add("secret", secret)
	query.Add("issuer", issuer)
	query.Add("user", user)
	baseURL.RawQuery = query.Encode()
	return baseURL.String(), nil
}

// GetQRImage -
func (c *Client) GetQRImage(secret, issuer, user string) ([]byte, error) {
	baseURL, err := c.GetQRUrl(secret, issuer, user)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", baseURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "image/png")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", userAgent)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, checkConnectError(err)
	}

	if resp.StatusCode != 200 {
		return nil, parseErrorResponse(resp)
	}

	return ioutil.ReadAll(resp.Body)

}

// ValidateOTP -
func (c *Client) ValidateOTP(secret, code string) error {
	partialURL, err := url.Parse("/v1/totp/validate")
	if err != nil {
		return err
	}
	query := url.Values{}
	query.Add("secret", secret)
	query.Add("code", code)
	partialURL.RawQuery = query.Encode()

	var result interface{}
	return c.get(partialURL.String(), &result)
}

*/

// RequestPushNotification requests a pust notification for the given user.
// Required parameters:
// - message string
// Optional parameters:
// - expires_in int (seconds) - default is 3600 (1 hr)
// - details
// - hidden_details
// - logos
func (c *Client) RequestPushNotification(userID int, params *Parameters) (*Notification, error) {
	var resp Response
	err := c.post(fmt.Sprintf("/v1/users/%d/push", userID), params, &resp)
	if err != nil {
		return nil, err
	}

	notification := &Notification{}
	err = fromMap(resp.Data, notification)
	if err != nil {
		return nil, err
	}

	return notification, nil
}

// CheckPushNotification checks the status of the notification
func (c *Client) CheckPushNotification(userID int, code string) (*NotificationStatus, error) {
	var resp Response
	err := c.get(fmt.Sprintf("/v1/users/%d/push/%s", userID, url.QueryEscape(code)), &resp)
	if err != nil {
		return nil, err
	}

	status := &NotificationStatus{}
	err = fromMap(resp.Data, status)
	if err != nil {
		return nil, err
	}

	return status, nil
}
